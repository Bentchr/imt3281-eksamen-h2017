package no.ntnu.imt3281.util;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;
import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

/**
 * Created in ordet to handle all actions with prefrences.
 * 
 * @author Bent_
 *
 */
public class StorageHandler {
	private static Preferences prefs;
	private static final String[] directories = {"w1280", "w500", "w780", "h623", "w300"};
	private static String path;
	
	/**
	 * Created in order to set file.
	 * 
	 * @param file File to be set.
	 */
	public static void setPath(File file) {
		prefs = Preferences.userNodeForPackage(StorageHandler.class);
		prefs.put("LAST_OUTPUT_DIR", file.getAbsolutePath());
		file.mkdir();
		
		for(int i = 0; i<directories.length; i++) {		// kode herfra tatt fra https://stackoverflow.com/questions/28947250/create-a-directory-if-it-does-not-exist-and-then-create-the-files-in-that-direct
			File directory = new File(file.getAbsolutePath()+"\\"+directories[i]);
			if(! directory.exists()) {
				directory.mkdir();
			}
		}
	}
	
	public static void getPrefrences() {
		prefs = Preferences.userNodeForPackage(StorageHandler.class);
		path = prefs.get("LAST_OUTPUT_DIR", "");
	}
	
	/**
	 * Function to write image to file.
	 * 
	 * @param image image to write.
	 * @param sizeAndName size and name of image.
	 */
	public static void writeToFile(Image image, String sizeAndName) {
		if(!path.equals("")) {
			try {
				File file = new File(path);
				if(file.exists()) {
					ImageIO.write(SwingFXUtils.fromFXImage(image, null), "jpg", new File(path+"\\"+sizeAndName));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Function to read image from file.
	 * 
	 * @param sizeAndName size and name of image.
	 * @return returns image if found, else null.
	 */
	public static Image readFromFile(String sizeAndName) {
		sizeAndName = sizeAndName.replace("//", "\\");
		try {
			File imageFile = new File(path+"\\"+sizeAndName);
			if(imageFile.exists()) {
				Image image = SwingFXUtils.toFXImage(ImageIO.read(imageFile), null);
				return image;
			}
			else
				return null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	/**
	 * Function to return path to images.
	 * 
	 * @return path to images.
	 */
	public static String getPath() {
		return path;
	}
}
