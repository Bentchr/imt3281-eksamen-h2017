package no.ntnu.imt3281.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Code to create a Derby database
 * 
 * @author Bent_
 */
public class CreateDB {						// OBS: Koden her er tatt fra Kontinuasjonseksamen v-2017.
		private static final String URL = "jdbc:derby:tvCollection;create=true";
		private static Connection con;
		
		/**
		 * Main function, creates database and calls for setup.
		 * @param args commandline args.
		 */
		public static void main(String[] args) {
			try {
	            con = DriverManager.getConnection(URL);
	            setupDB();
	            System.out.println("DB created");
			} catch (SQLException sqle) {		// No database exists
				try {							// Try creating database
					con = DriverManager.getConnection(URL+";create=true");
				} catch (SQLException sqle1) {	// Unable to create database, exit server
					System.err.println("Unable to create database"+sqle1.getMessage());
					System.exit(-1);
				}
			}
		}									// OBS: Kopiert kode ender her.
		
		private static void setupDB() throws SQLException {			// OBS: hele denne funksjonen er inspirert av Kontinuasjonseksamen v-2017.
			Statement stmt = con.createStatement();
			stmt.execute("CREATE TABLE genres (id INTEGER NOT NULL,"
	                + "genre varchar(255) NOT NULL, "
	                + "PRIMARY KEY  (id))");
			stmt.execute("CREATE TABLE movies(id INTEGER NOT NULL,"
					+ "JSONdata long varchar NOT NULL,"
					+ "PRIMARY KEY (id))");
			stmt.execute("CREATE TABLE actors(id INTEGER NOT NULL,"
					+ "JSONdata long varchar NOT NULL,"
					+ "PRIMARY KEY (id))");
			stmt.execute("CREATE TABLE takesPartIn(id INTEGER NOT NULL,"
					+ "JSONdata long varchar NOT NULL,"
					+ "PRIMARY KEY (id))");
			stmt.close();
		}
}
