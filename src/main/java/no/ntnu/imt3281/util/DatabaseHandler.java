package no.ntnu.imt3281.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import no.ntnu.imt3281.movieExplorer.JSON;


/**
 * Created in order to handle all database queries.
 * 
 * @author Bent_
 *
 */
public class DatabaseHandler {
	private static String dbURL = "jdbc:derby:tvCollection";
	
	/**
	 * Takes a JSON object with genres and
	 * inserts them into the DB.
	 * @param genres JSON object with genres.
	 */
	public static void updateGenres(JSON genres) {
		JSON temp;
		int id;
		String genreName;
		String sql;
		
		try {
			Connection con = DriverManager.getConnection(dbURL);
			PreparedStatement stmnt;
			for(int i = 0; i<genres.get("genres").size(); i++){
				temp = genres.get("genres").get(i);
				 id = Integer.parseInt(temp.getValue("id").toString());
				 if(getGenre(id) == null) {
					 genreName = temp.getValue("name").toString();
					 sql = "INSERT INTO genres (id, genre) VALUES (?, ?)";
					 stmnt = con.prepareStatement(sql);
					 stmnt.setInt(1, id);
					 stmnt.setString(2, genreName);
					 stmnt.execute();			
				 }
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Function to get Genre from DB.
	 * 
	 * @param id of the genre.
	 * @return name of the genre.
	 */
	public static String getGenre(int id) {
		try {
			Connection con = DriverManager.getConnection(dbURL);
			String sql = "SELECT * FROM genres WHERE id = ?";
			PreparedStatement stmnt = con.prepareStatement(sql);
			stmnt.setInt(1, id);
	        ResultSet res = stmnt.executeQuery();
	        
	        if(res.next()) {
	        	return res.getString(2);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Function to insert data with attributes id and JSON data into database.
	 * 
	 * @param id id of given JSON data as int.
	 * @param json JSON data as String
	 * @param tableName name of the table data is inserted.
	 */
	public static void insertData(int id, String json, String tableName) {
		String sql;
		try {
			Connection con = DriverManager.getConnection(dbURL);
			PreparedStatement stmnt;
			sql = "INSERT INTO "+tableName+" (id, JSONdata) VALUES (?, ?)";
			stmnt = con.prepareStatement(sql);
			stmnt.setInt(1, id);
			stmnt.setString(2, json);
			stmnt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Function to select data from database.
	 * 
	 * @param id id of data to select.
	 * @param tableName name of table to select from.
	 * @return JSON data or null if not found.
	 */
	public static String selectData(int id, String tableName) {
		try {
			Connection con = DriverManager.getConnection(dbURL);
			String sql = "SELECT * FROM "+tableName+" WHERE id = ?";
			PreparedStatement stmnt;
			stmnt = con.prepareStatement(sql);
			stmnt.setInt(1, id);
			ResultSet res = stmnt.executeQuery();
		
			if(res.next()) {
				return res.getString(2);
			}
			return null;
		} catch (SQLException e) {
		e.printStackTrace();
		return null;
		}	
	}
	
	/**
	 * Function to clean the enitre database
	 */
	public static void cleanDB() {
		try {
			Connection con = DriverManager.getConnection(dbURL);
			Statement stmnt = con.createStatement();
			stmnt.executeQuery("DELETE FROM genres WHERE 1=1");
			stmnt.executeQuery("DELETE FROM movies WHERE 1=1");
			stmnt.executeQuery("DELETE FROM actors WHERE 1=1");
			stmnt.executeQuery("DELETE FROM takesPartIn WHERE 1=1");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
