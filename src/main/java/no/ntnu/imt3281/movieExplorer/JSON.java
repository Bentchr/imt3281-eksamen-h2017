package no.ntnu.imt3281.movieExplorer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Klassen er laget for å forenkle bruken av JSON data. 
 * 
 * @author Bent_
 *
 */
public class JSON {
	private JSONObject dataObject;
	private JSONArray dataArray;
	private int length;
	
	/**
	 * Tar imot JSON data som en String og parser dette som et JSONObject eller JSONArray.
	 * @param jsonInput input som inneholder JSON data.
	 */
	public JSON(String jsonInput) {
		JSONParser jParser = new JSONParser();
		try {
			Object parsedObject = jParser.parse(jsonInput);
			if(parsedObject instanceof JSONObject) {
				dataObject = (JSONObject) parsedObject;
				length = dataObject.size();
			}
			else if(parsedObject instanceof JSONArray) {
				dataArray = (JSONArray) parsedObject;
				length = dataArray.size();
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Takes a String value, finds the value in given JSON data and returns
	 * it as an Object.
	 * 
	 * @param value Value to find.
	 * @return found Value as an Object.
	 */
	public Object getValue(String value) {
		return dataObject.get(value);
	}
	
	/**
	 * Takes a String value, finds the value in given JSON data and returns
	 * it as an JSON object.
	 * 
	 * @param value Value to find.
	 * @return found Value as JSON object.
	 */
	public JSON get(String value) {
		return new JSON(getValue(value).toString());
	}
	
	/**
	 * Takes a number Index, finds the value at the given index and returns
	 * it as an JSON object.
	 * 
	 * @param index index to find.
	 * @return found value at given index.
	 */
	public JSON get(int index) {
		return new JSON(dataArray.get(index).toString());
	}
	
	/**
	 * returns size of JSON object.
	 * 
	 * @return size of JSON object.
	 */
	public int size() {
		return length;
	}
}
