package no.ntnu.imt3281.movieExplorer;




import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import no.ntnu.imt3281.util.DatabaseHandler;
import no.ntnu.imt3281.util.StorageHandler;

/**
 * Controller class for the FXML file GUI.
 * @author Bent_
 *
 */
public class GUI {
    @FXML private TextField searchField;
    @FXML private TreeView<SearchResultItem> searchResult;
    @FXML private Pane detailPane;
    private DetailGUI controller;
    private ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.I18N");
    
    // Root node for search result tree view
    private TreeItem<SearchResultItem> searchResultRootNode = new TreeItem<SearchResultItem> (new SearchResultItem(""));
    
    @FXML
    /**
     * Called when the object has been created and connected to the fxml file. All components defined in the fxml file is 
     * ready and available.
     */
    public void initialize() {
    		searchResult.setRoot(searchResultRootNode);
    		searchResult.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> { 				// Denne ene linjen er hentet fra https://stackoverflow.com/questions/31897452/javafx-treeview-listener
    			SearchResultItem temp = (SearchResultItem) searchResult.getSelectionModel().getSelectedItem().getValue();
    			//System.out.println(temp.getMediaType());	//Laget for oppgave 5. skal fjernes i oppgave 6, lar stå som kommentar.
    			
    			//Oppgave 6:
    			if(temp.getMediaType().equals("person")) {
    				JSON result = Search.takesPartIn(temp.getID().intValue()).get("results");
    				for(int i = 0; i<result.size(); i++) {
    					SearchResultItem item = new SearchResultItem(result.get(i));
    					searchResult.getSelectionModel().getSelectedItem().getChildren().add(new TreeItem<SearchResultItem>(item));
    				}
    				searchResult.getSelectionModel().getSelectedItem().setExpanded(true);
    			}
    			else if (temp.getMediaType().equals("movie")) {
    				JSON result = Search.actors(temp.getID().intValue()).get("cast");
    				for(int i = 0; i<result.size(); i++) {
    					SearchResultItem item = new SearchResultItem(result.get(i));
    					searchResult.getSelectionModel().getSelectedItem().getChildren().add(new TreeItem<SearchResultItem>(item));
    				}
    				//Oppgave 7:
    				try {
    					if(controller == null) {
    						FXMLLoader loader = new FXMLLoader(getClass().getResource("DetailGUI.fxml"), bundle);
    						detailPane.getChildren().add(loader.load());
    						controller = loader.<DetailGUI>getController();
    					}
    						controller.setData(Search.movie(temp.getID()));
					} catch (IOException e) {
						e.printStackTrace();
					}
    			}
    			});
    }

    @FXML
    /**
     * Called when the seqrch button is pressed or enter is pressed in the searchField.
     * Perform a multiSearch using theMovieDB and add the results to the searchResult tree view.
     * 
     * @param event ignored
     */
    void search(ActionEvent event) {
    		JSON result = Search.multiSearch(searchField.getText()).get("results");
    		TreeItem<SearchResultItem> searchResults = new TreeItem<> (new SearchResultItem("Searching for : "+searchField.getText()));
    		searchResultRootNode.getChildren().add(searchResults);
    		for (int i=0; i<result.size(); i++) {
    			SearchResultItem item = new SearchResultItem(result.get(i));
    			searchResults.getChildren().add(new TreeItem<SearchResultItem>(item));
    		}
    		searchResultRootNode.setExpanded(true);
    		searchResults.setExpanded(true);
    }
    
    @FXML
    /**
     * Function to get filechooser.
     * 
     * @author Bent_
     *
     */
    public void openFileChooser(ActionEvent ae) {
    	FileChooser fileChooser = new FileChooser();		// Koden herfa er tatt fra https://docs.oracle.com/javafx/2/ui_controls/file-chooser.htm
    	fileChooser.setTitle("Largrings sted for bilder");
    	StorageHandler.setPath(fileChooser.showSaveDialog(null));					// til hit.
    }
    
    @FXML
    /**
     * Function to open aboutMenu.
     * 
     */
    public void openHelpMenu() {
    	Dialog dialog = new Dialog();
    	dialog.setTitle(bundle.getString("aboutExplorer"));
    	dialog.setHeaderText(bundle.getString("aboutExplorer"));
    	dialog.setResizable(true); 
    	
    	Label discUsage = new Label(bundle.getString("discUsage"));
    	
    	File f = new File("D:\\eclipse-workspace\\imt3281-eksamen-h2017\\tvCollection");
    	Label database = new Label(bundle.getString("database"));
    	Label dbSize = new Label(String.valueOf(f.length()/1000)+" KiB");
    	
    	f = new File(StorageHandler.getPath()+"\\w300\\");
    	Label posterImages = new Label(bundle.getString("posterImages"));
    	Label posterSize = new Label(String.valueOf(f.length())+" KiB");
    	
    	f = new File(StorageHandler.getPath()+"\\h623\\");
    	Label profileImages = new Label(bundle.getString("profileImages"));
    	Label profileSize = new Label(String.valueOf(f.length())+" KiB");
    	
    	f = new File(StorageHandler.getPath()+"\\w500\\");
    	Label backdropImages = new Label(bundle.getString("backdropImages"));
    	Label backdropSize = new Label(String.valueOf(f.length())+" KiB");
    	
    	f = new File(StorageHandler.getPath()+"\\w780\\");
    	Label logoImages = new Label(bundle.getString("logoImages"));
    	Label logoSize = new Label(String.valueOf(f.length())+" KiB");
    	
    	f = new File(StorageHandler.getPath()+"\\w1280\\");
    	Label stilImages = new Label(bundle.getString("stilImages"));
    	Label stillSize = new Label(String.valueOf(f.length())+" KiB");
    	
    	
    	Button emptyDB = new Button(bundle.getString("emptyDB"));
    	emptyDB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DatabaseHandler.cleanDB();
            }
        });
    	
    	Button emptyCache = new Button(bundle.getString("emptyCache"));
    	emptyCache.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	File file = new File(StorageHandler.getPath());
            	try {
					delete(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
            }
        });
    	Button okButton = new Button(bundle.getString("okButton"));
    	okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);
            	dialog.close(); 
            }
        });
    	
    	GridPane grid = new GridPane();
    	grid.add(discUsage, 1, 1);
    	
    	grid.add(database, 1, 2);
    	grid.add(dbSize, 2, 2);
    	
    	grid.add(posterImages, 1, 3);
    	grid.add(posterSize, 2, 3);
    	
    	grid.add(profileImages, 1, 4);
    	grid.add(profileSize, 2, 4);
    	
    	grid.add(backdropImages, 1, 5);
    	grid.add(backdropSize, 2, 5);
    	
    	grid.add(logoImages, 1, 6);
    	grid.add(logoSize, 2, 6);
    	
    	grid.add(stilImages, 1, 7);
    	grid.add(stillSize, 2, 7);
    	
    	grid.add(emptyDB, 1, 8);
    	grid.add(emptyCache, 2, 8);
    	grid.add(okButton, 3, 9);
    	dialog.getDialogPane().setContent(grid);
    	dialog.show();
    }
    
	private static void delete(File file) throws IOException {	// Hele funksjonen er kopiert fra http://roufid.com/how-to-delete-folder-recursively-in-java/
		 
		for (File childFile : file.listFiles()) {
 
			if (childFile.isDirectory()) {
				delete(childFile);
			} else {
				if (!childFile.delete()) {
					throw new IOException();
				}
			}
		}
 
		if (!file.delete()) {
			throw new IOException();
		}
	}
 
    /**
     * Class for storing searchresults easier.
     * @author Bent_
     *
     */
    class SearchResultItem {
    		private String media_type = "";
    		private String name = "";
    		private long id;
    		private String profile_path = "";
    		private String title = ""; 
    		
    		/**
    		 * Create new SearchResultItem with the given name as what will be displayed in the tree view.
    		 * 
    		 * @param name the value that will be displayed in the tree view
    		 */
    		public SearchResultItem(String name) {
    			this.name = name;
    		}
    		
    		/**
    		 * Create a new SearchResultItem with data form this JSON object.
    		 * 
    		 * @param json contains the data that will be used to initialize this object.
    		 */
		public SearchResultItem(JSON json) {
			if(json.getValue("media_type") == null) {
				if(json.getValue("title") != null) {
					media_type = "movie";
				} else {
					media_type = "person";
				}
			} else {
				media_type = (String) json.getValue("media_type");
			}
			if (media_type.equals("person")) {
				name = (String)json.getValue("name");	
				profile_path = (String)json.getValue("profile_path");
			} else if (media_type.equals("movie")) {
				title = (String)json.getValue("title");
			} else if (media_type.equals("tv")) {
				name = (String)json.getValue("name");
			}
			id = (Long)json.getValue("id");
		}
		
		/**
		 * Function to get media type.
		 * 
		 * @return media type.
		 */
		public String getMediaType() {
			return media_type;
		}
		
		/**
		 * Function to get id
		 * 
		 * @return id
		 */
		public Long getID() {
			return id;
		}
    		
		/**
		 * Used by the tree view to get the value to display to the user. 
		 */
		@Override
		public String toString() {
			if (media_type.equals("person")) {
				return name;
			} else if (media_type.equals("movie")) {
				return title;
			} else {
				return name;
			}
		}
    }
}
