package no.ntnu.imt3281.movieExplorer;

import java.io.IOException;
import java.net.MalformedURLException;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;

/**
 * Controller class for the fxml file DetailGUI.
 * @author Bent_
 *
 */
public class DetailGUI {
	@FXML private Label title;
	@FXML private Label genres;
	@FXML private TextArea movieInfo;
	@FXML private ImageView image;
	
	/**
	 * Constructor for DetailGUI. Takes JSON movie data and sets values
	 * for all fields based on that data.
	 * 
	 * @param data JSON movie data.
	 */
	public void setData(JSON data) {
		String genresText = "";
		title.setText((String) data.getValue("title"));
		movieInfo.setText((String) data.getValue("overview"));
		JSON result = data.get("genres");
		
		//genresText += result.get(0).getValue("name");
		for(int i = 0; i<result.size(); i++) {
			genresText+=result.get(i).getValue("name")+"\n";
		}
		genres.setText(genresText);
		
		TheMovieDBConfiguration configuration = new TheMovieDBConfiguration(Search.getImageConfig());
		image.setImage(configuration.getImage(configuration.getStillURL((String)data.getValue("poster_path"))));
		
		// Under her er kode fra før Oppgave 9:
		//setImage((String)data.getValue("poster_path"));
	}
	/*
	private void setImage(String poster_path) {
		TheMovieDBConfiguration configuration = new TheMovieDBConfiguration(Search.getImageConfig());
		image.setImage(configuration.getImage(configuration.getStillURL(poster_path)));		
		
		//Kode fra før oppgave 9.
		//Image cover = new Image(configuration.getStillURL(poster_path));	// Kopiert kode fra kontinuasjonseksamen-v 2017
		//Platform.runLater(new Runnable() {	// Bruker tråd her fordi det kan ta tid å hente et bilde.
		//	public void run() {
		//		image.setImage(cover);
		//	}
		//	});																// kopiert kode ender her.
		
	}*/
}
