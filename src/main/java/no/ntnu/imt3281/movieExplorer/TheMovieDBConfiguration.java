package no.ntnu.imt3281.movieExplorer;

import java.util.Iterator;
import org.json.simple.JSONArray;
import javafx.scene.image.Image;
import no.ntnu.imt3281.util.StorageHandler;

/**
 * Class to get the correct URL for image size. Gets the configuration.
 * @author Bent_
 *
 */
public class TheMovieDBConfiguration {
	private JSON data;
	private String baseURL;
	
	
	/**Constructor. sets the JSON data.
	 * 
	 * @param json JSON data as String.
	 */
	public TheMovieDBConfiguration(String json) {
		data = new JSON(json);
		baseURL = data.get("images").getValue("base_url").toString();
	}
	
	public TheMovieDBConfiguration(JSON data) {
		this.data = data;
		baseURL = data.get("images").getValue("base_url").toString();
	}
	
	/**
	 * Gets configuration for backdrop sizes.
	 * 
	 * @param imName image name.
	 * @return	correct image URL.
	 */
	public String getBackdropURL(String imName) {
		
		JSONArray temp = (JSONArray) data.get("images").getValue("backdrop_sizes");
		return getBiggest(temp)+"/"+imName;
	}
	
	/**
	 * Gets configuration for logo sizes.
	 * 
	 * @param imName image name.
	 * @return correct image URL.
	 */
	public String getLogoURL(String imName) {
		JSONArray temp = (JSONArray) data.get("images").getValue("logo_sizes");
		return getBiggest(temp)+"/"+imName;
	}
	
	/**
	 * Gets configuration for poster sizes.
	 * 
	 * @param imName image name.
	 * @return correct image URL.
	 */
	public String getPosterURL(String imName) {
		JSONArray temp = (JSONArray) data.get("images").getValue("poster_sizes");
		return getBiggest(temp)+"/"+imName;
	}
	
	/**
	 * Gets configuration for profile sizes.
	 * 
	 * @param imName image name.
	 * @return correct image URL.
	 */
	public String getProfileURL(String imName) {
		JSONArray temp = (JSONArray) data.get("images").getValue("profile_sizes");
		return getBiggest(temp)+"/"+imName;
	}
	
	/**
	 * Gets configuration for still sizes.
	 * 
	 * @param imName image name.
	 * @return correct image URL.
	 */
	public String getStillURL(String imName) {
		JSONArray temp = (JSONArray) data.get("images").getValue("still_sizes");
		return getBiggest(temp)+"/"+imName;
	}
	
	/**
	 * Searches database for image. If image not found, searches the web
	 * for image. If found stored locally, if not null is returned.
	 * 
	 * @param poster_path WEB path to image.
	 * @return returns image if found, else null
	 */
	public Image getImage(String poster_path) {
		String sizeAndName = poster_path.replace(data.get("images").getValue("base_url").toString(), "");
		Image image = StorageHandler.readFromFile(sizeAndName);
		
		if(image == null) {
			image = new Image(poster_path);
			StorageHandler.writeToFile(image, sizeAndName);
		}
		return image;

	}
	
	private String getBiggest(JSONArray jArray) {	
		String largest = null;
		Iterator<String> iterator = jArray.iterator();
		while(iterator.hasNext()) {
			Object o = iterator.next();
			if(o.toString().equals("original")) {
				return baseURL+largest;
			}
			else {
				largest = o.toString();
			}
		}
		return null;
	}
}
