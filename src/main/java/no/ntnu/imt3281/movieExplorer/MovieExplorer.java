package no.ntnu.imt3281.movieExplorer;

import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import no.ntnu.imt3281.util.DatabaseHandler;
import no.ntnu.imt3281.util.StorageHandler;

/**
 * Main class for the application.
 * Initializes the app and starts the gui.
 * @author Bent_
 *
 */
public class MovieExplorer extends Application {
	
	@Override
	/**
	 * Starts the GUI.
	 */
	public void start(Stage primaryStage) throws Exception {
		initializaApp();
		ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.I18N");
		primaryStage.setTitle("Movie explorer");
		AnchorPane gui = (AnchorPane) FXMLLoader.load(getClass().getResource("GUI.fxml"), bundle);
		Scene myScene = new Scene(gui);
		primaryStage.setScene(myScene);
		primaryStage.show();
	}
	
	/**
	 * Main function.
	 * @param args command-line args
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	private void initializaApp() {
		StorageHandler.getPrefrences();
		JSON movieGenres = Search.getMovieGenres();		// Disse 4 linjene oppdaterer databasen.
		DatabaseHandler.updateGenres(movieGenres);		// Må flyttes til et annet sted hvis databasen bare skal
		JSON tvGenres = Search.getTvGenres();			// Oppdateres da programmet starter.
		DatabaseHandler.updateGenres(tvGenres);
	}
}
