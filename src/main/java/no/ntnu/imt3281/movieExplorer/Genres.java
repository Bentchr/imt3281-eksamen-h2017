package no.ntnu.imt3281.movieExplorer;

import no.ntnu.imt3281.util.DatabaseHandler;
import no.ntnu.imt3281.util.StorageHandler;

/**
 * Class is created in order to deal with genres.
 * @author Bent_
 *
 */
public class Genres {

	/**
	 * Function to get a gendre mapped to a ID from DB
	 * 
	 * @param id Genre Id.
	 * @return returns name of gendre.
	 */
	public static String resolve(int id) {	
		String resolve = DatabaseHandler.getGenre(id);
		if(resolve == null) {
			JSON movieGenres = Search.getMovieGenres();		// Disse 4 linjene oppdaterer databasen.
			JSON tvGenres = Search.getTvGenres();			// Oppdateres da programmet starter.
			
			JSON movieArray = movieGenres.get("genres");
			for(int i = 0; i<movieArray.size(); i++) {
				int temp = Integer.parseInt(movieArray.get(i).getValue("id").toString());
				if(temp == id) {
					return (String) movieArray.get(i).getValue("name");
				}
			}
			
			JSON tvArray = tvGenres.get("genres");
			for(int i = 0; i<tvArray.size(); i++) {
				int temp = Integer.parseInt(tvArray.get(i).getValue("id").toString());
				if(temp == id) {
					return (String) tvArray.get(i).getValue("name");
				}
			}
			
		}
		return resolve;
	}
}
