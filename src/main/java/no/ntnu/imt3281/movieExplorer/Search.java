package no.ntnu.imt3281.movieExplorer;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import no.ntnu.imt3281.util.DatabaseHandler;

/**
 * Class to handle all querys to TheMovieDB.
 * 
 * @author Bent_
 *
 */
public class Search {
	private static String api_key = "a47f70eb03a70790f5dd711f4caea42d";

	/**
	 * Function to query a multisearch on TheMovieDB. returns the query result as JSON object.
	 * 
	 * @param value search word.
	 * @return JSON data as JSON object.
	 */
	public static JSON multiSearch(String value) {
		String url = "https://api.themoviedb.org/3/search/multi?api_key="+api_key+"&language=en-US&query="+value.replace(" ", "%20")+"&include_adult=false";
		return new JSON(makeQuery(url));
	}
	
	/**
	 * Function to query a actors search on TheMovieDB or database. returns the query result as JSON object.
	 * 
	 * @param id Id of actor.
	 * @return JSON data as JSON object.
	 */
	public static JSON actors(int id) {
		String url = "https://api.themoviedb.org/3/movie/"+id+"/credits?api_key="+api_key;
		String tableName = "actors";
		String data = DatabaseHandler.selectData(id, tableName);
		
		if(data == null) {
			data = makeQuery(url);
			DatabaseHandler.insertData((int) id, data, "actors");
		}
		return new JSON(data);
	}
	
	/**
	 * Function to query a takesPartIn search on TheMovieDB or database. Returns the query result as JSON object
	 * 
	 * @param id movie id
	 * @return JSON data with result of query.
	 */
	public static JSON takesPartIn(int id) {
		String url = "https://api.themoviedb.org/3/discover/movie?api_key="+api_key+"&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_people="+id;
		String tableName = "takesPartIn";
		String data = DatabaseHandler.selectData(id, tableName);
		
		if(data == null) {
			data = makeQuery(url);
			DatabaseHandler.insertData(id, data, tableName);
		}
		return new JSON(data);
	}
	
	/**
	 * Function to query Tv show genres search on theMovieDB. Returns query result as JSON object.
	 * @return JSON data as JSON object.
	 */
	public static JSON getTvGenres() {
		String url = "https://api.themoviedb.org/3/genre/tv/list?api_key="+api_key;
		return new JSON(makeQuery(url));
	}
	
	/**
	 * Function to query Movie genres search on theMovieDB. Returns query result as JSON object.
	 * @return JSON data as JSON object.
	 */
	public static JSON getMovieGenres() {
		String url = "https://api.themoviedb.org/3/genre/movie/list?api_key="+api_key;
		return new JSON(makeQuery(url));
	}
	
	/**
	 * Function to query movie search on theMovieDB or database. Returns query result as JSON object.
	 * 
	 * @param id Movie id
	 * @return JSON data as JSON object.
	 */
	public static JSON movie(long id) {
		String url = "https://api.themoviedb.org/3/movie/"+id+"?api_key="+api_key+"&language=en-US";
		String tableName = "movies";
		String data = DatabaseHandler.selectData((int) id, tableName);
		if(data == null) {
			data = makeQuery(url);
			DatabaseHandler.insertData((int) id, data, tableName);
		}
		return new JSON(data);
	}
	
	/**
	 * Function to query configurations for TheMovieDB. Returns query result as JSON object.
	 * 
	 * @return JSON data as JSON object.
	 */
	public static JSON getImageConfig() {
		String url = "https://api.themoviedb.org/3/configuration?api_key="+api_key;
		return new JSON(makeQuery(url));
	}
	
	private static String makeQuery(String url) {
		String data;
		try {
			data= Unirest.get(url).asString().getBody();
			return data;
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}
}
